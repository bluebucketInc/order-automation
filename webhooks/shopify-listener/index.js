
// imports
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const portToListenOn = 3800;

// app headers
app.use(bodyParser.json());
app.use(
	bodyParser.urlencoded({
		extended: true
	})
);

/*
	In your Shopify store
	Settings -> Notifications
	Click "Create a webhook"
	- Choose 'Order Created'
	- Leave the format as JSON
	- Set the URL with IP address to production server -> http://123.345.11.22:3800/bolon-order-listener
*/
app.post("/bolon-order-listener", (req, res) => {

	// response status
	res.send("OK");

	// order data recieved
	const theData = req.body;
	console.log(theData);

});


// Run server 
app.listen(portToListenOn, () => console.log(`Listening for Shopify webhook event data on port ${portToListenOn}`));